const { response } = require("express");
const express = require("express");
const app = express();

app.use(express.json());

app.get("/funcionario", (req, res) => {
  res.send("chegou no funcionario");
});

app.get("/pesquisa", (req, res) => {
  const dados = req.query;

  console.log(dados);

  res.send("recebi");
});

app.get("/pesquisa2", (req, res) => {
  const query = req.query;

  const nomeGrande = query.nome.toUpperCase();

  res.send(nomeGrande);
});

app.post("/pesquisa3", (req, res) => {
  const body = req.body;

  res.send(body.valor);
});

// 1º.) parâmetro de envio via query (atributo nome)
app.get("/clientes", (req, res) => {
  let query = req.query;

  res.send("recebido");

  console.log(query.nome);
});

// 1º) criar um parâmetro no header chamado access;
//2º) parâmetros de envio via body;
//3º) validar se o valor que foi enviado no header está correto para prosseguir com a execução;



app.post("/clientes", (req, res) => {
  const header = req.headers;
  const body = req.body;
  

  const retorno = { access: header.access, body: body.message };

    if (header.access){
        res.send(retorno);
        
    }else{
        res.send("Invalido")
    }
});
    

app.delete("/clientes/:id", (req, res) => {
    const header = req.headers;
    const params = req.params;
    
   
    const retorno = { access: header.access, id: params.id};
    
        if (header.access){
            res.send(retorno);
            
        }else{
            res.send("INVÁLIDO");
        }
});

app.put("/clientes/:id", (req, res) => {
    const header = req.headers;
    const params = req.params;
    
   
    const retorno = { access: header.access, id: params.id };
    
        if (header.access){
            res.send(retorno);
            
        }else{
            res.send("INVÁLIDO");
        }
});



const PORT = 8000;
app.listen(PORT, () => {
  console.log("rodando na porta", PORT);
});
